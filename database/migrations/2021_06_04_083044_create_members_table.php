<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('father_name')->nullable();
            $table->string('father_job')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('mother_job')->nullable();
            $table->string('gender');
            $table->string('blood_type')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('mobile');
            $table->string('landline')->nullable();
            $table->string('email')->nullable();
            $table->integer('zone_number')->nullable();
            $table->integer('region_number')->nullable();
            $table->string('voting_caza')->nullable();
            $table->string('voting_city')->nullable();
            $table->string('residence_address')->nullable();
            $table->string('school')->nullable();
            $table->string('academic_level')->nullable();
            $table->string('university')->nullable();
            $table->string('major')->nullable();
            $table->string('faculty')->nullable();
            $table->boolean('is_working')->default(0);
            $table->string('where_working')->nullable();
            $table->boolean('is_syndicate')->default(0);
            $table->string('syndicate')->nullable();
            $table->string('syndicate_number')->nullable();
            $table->string('political_status')->nullable();
            $table->integer('card_number')->nullable();
            $table->string('source');
            $table->string('reference')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
