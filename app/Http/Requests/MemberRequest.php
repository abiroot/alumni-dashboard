<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'father_name' => 'min:3|max:255',
            'father_job' => 'min:3|max:255',
            'mother_name'=>'min:3|max:255',
            'mother_job'=>'min:3|max:255',
            'gender' => 'required',
            'date_of_birth'=>'before:today',
            'mobile'=>'required|min:8|max:15',
            'landline'=>'min:8|max:15',
            'email'=>'email',
            'is_working'=>'required|boolean',
            'where_working'=>'min:3|max:255',
            'is_syndicate'=>'required|bool',
            'syndicate_number'=>'numeric',
            'source'=>'required|min:3|max:255',
            'reference'=>'min:3|max:255'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
