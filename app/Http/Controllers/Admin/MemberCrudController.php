<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MemberRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MemberCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MemberCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Member::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/member');
        CRUD::setEntityNameStrings('member', 'members');
        $this->crud->enableExportButtons();
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */

        $this->crud->addFilter([
            'name'  => 'voting_caza',
            'type'  => 'select2',
            'label' => 'Voting Caza'
        ], function() {
            return [
                'beirut1'=>'Beirut I',
                'beirut2'=>'Beirut II',
                'maten'=>'Maten',
                'baabda'=>'Baabda',
                'aley'=>'Aley',
                'chouf'=>'Chouf',
                'jbeil'=>'Jbeil',
                'keserwan'=>'Keserwan',
                'zahle'=>'Zahle',
                'west bekaa'=>'West Bekaa',
                'rachaya'=>'Rachaya',
                'hermel'=>'Hermel',
                'baalbak'=>'Baalbak',
                'hasbaya'=>'Hasbaya',
                'saida'=>'Saida',
                'sour'=>'Sour',
                'jezzine'=>'Jezzine',
                'nabatiyeh'=>'Nabatiyeh',
                'marjeyoun'=>'Marjeyoun',
                'bent jbeil'=>'Bent Jbeil',
                'zahrani'=>'Zahrani',
                'akkar'=>'Akkar',
                'tripoli'=>'Tripoli',
                'zgharta'=>'Zgharta',
                'bcharre'=>'Bcharre',
                'batroun'=>'Batroun',
                'koura'=>'Koura',
                'danniyeh-menniyeh'=>'Danniyeh - Menniyeh'
            ];
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'voting_caza', $value);
        });

        $this->crud->addFilter(
            [
                'type'  => 'simple',
                'name'  => 'isFPM_card_holder',
                'label' => 'FPM Card Holder'
            ],
            false,
            function() { // if the filter is active
                $this->crud->addClause('where', 'card_number', !null);
            }
        );

        $this->crud->addFilter([
            'name'  => 'blood_type',
            'type'  => 'select2',
            'label' => 'Blood Type'
        ], function() {
            return ['O+' => 'O+', 'O-' => 'O-', 'A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-'];
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'blood_type', $value);
        });

    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MemberRequest::class);

        CRUD::field('first_name');
        CRUD::field('last_name');
        CRUD::field('father_name');
        CRUD::field('father_job');
        CRUD::field('mother_name');
        CRUD::field('mother_job');
        CRUD::addField([   // select2_from_array
            'name'        => 'gender',
            'label'       => "Gender",
            'type'        => 'select2_from_array',
            'options'     => ['male' => 'Male', 'female' => 'Female'],
            'allows_null' => false,
        ]);
        CRUD::addField([   // select2_from_array
            'name'        => 'blood_type',
            'label'       => "Blood Type",
            'type'        => 'select2_from_array',
            'options'     => ['O+' => 'O+', 'O-' => 'O-', 'A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-'],
            'allows_null' => true,
        ]);
        CRUD::addField([
            'name'  => 'date_of_birth',
            'label' => 'Date Of Birth',
            'type'  => 'date',
            'date_picker_options' => [
                'todayBtn' => 'false',
                'format'   => 'dd-mm-yyyy'
            ]
        ]);
        CRUD::field('mobile');
        CRUD::field('landline');
        CRUD::field('email');
        CRUD::addField([   // Number
            'name' => 'zone_number',
            'label' => 'Zone #',
            'type' => 'number'
        ]);
        CRUD::addField([   // Number
            'name' => 'zone_number',
            'label' => 'Zone #',
            'type' => 'number'
        ]);
        CRUD::addField([   // Number
            'name' => 'region_number',
            'label' => 'Region #',
            'type' => 'number'
        ]);
        CRUD::addField([   // select2_from_array
            'name'        => 'voting_caza',
            'label'       => "Voting Caza",
            'type'        => 'select2_from_array',
            'options'     => [
                'beirut1'=>'Beirut I',
                'beirut2'=>'Beirut II',
                'maten'=>'Maten',
                'baabda'=>'Baabda',
                'aley'=>'Aley',
                'chouf'=>'Chouf',
                'jbeil'=>'Jbeil',
                'keserwan'=>'Keserwan',
                'zahle'=>'Zahle',
                'west bekaa'=>'West Bekaa',
                'rachaya'=>'Rachaya',
                'hermel'=>'Hermel',
                'baalbak'=>'Baalbak',
                'hasbaya'=>'Hasbaya',
                'saida'=>'Saida',
                'sour'=>'Sour',
                'jezzine'=>'Jezzine',
                'nabatiyeh'=>'Nabatiyeh',
                'marjeyoun'=>'Marjeyoun',
                'bent jbeil'=>'Bent Jbeil',
                'zahrani'=>'Zahrani',
                'akkar'=>'Akkar',
                'tripoli'=>'Tripoli',
                'zgharta'=>'Zgharta',
                'bcharre'=>'Bcharre',
                'batroun'=>'Batroun',
                'koura'=>'Koura',
                'danniyeh-menniyeh'=>'Danniyeh - Menniyeh'
            ],
            'allows_null' => true,
        ]);
        CRUD::field('voting_city');
        CRUD::field('residence_address');
        CRUD::field('school');
        CRUD::addField([   // select2_from_array
            'name'        => 'academic_level',
            'label'       => "Academic Level",
            'type'        => 'select2_from_array',
            'options'     => [
                'none'=>'No Education',
                'not studying'=>'Not Studying',
                'technique'=>'Technique',
                'university'=>'University',
                'graduated'=>'Graduated'
            ],
            'allows_null' => true,
        ]);
        CRUD::field('university');
        CRUD::field('major');
        CRUD::field('faculty');
        CRUD::addField([   // Checkbox
            'name'  => 'is_working',
            'label' => 'Are you working',
            'type'  => 'checkbox'
        ]);
        CRUD::addField([
            'name'  => 'where_working',
            'label' => "Where are you working",
            'type'  => 'text'
        ]);
        CRUD::addField([   // Checkbox
            'name'  => 'is_syndicate',
            'label' => 'Are you a part of a syndicate',
            'type'  => 'checkbox'
        ]);
        CRUD::addField([
            'name'  => 'syndicate',
            'label' => "Which syndicate",
            'type'  => 'text'
        ]);
        CRUD::addField([
            'name'  => 'syndicate_number',
            'label' => "What is your syndicate number",
            'type'  => 'text'
        ]);
        CRUD::addField([
            'name'  => 'political_status',
            'label' => "Political Status (FPM)",
            'type'  => 'text'
        ]);
        CRUD::addField([
            'name'  => 'card_number',
            'label' => "FPM card number",
            'type'  => 'number'
        ]);
        CRUD::addField([
            'name'  => 'source',
            'label' => 'Source',
            'type'  => 'text'
        ]);
        CRUD::addField([
            'name'  => 'reference',
            'label' => 'Reference',
            'type'  => 'text'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
